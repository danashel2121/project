<?php
declare(strict_types=1);
namespace Magebit\Faq\Controller\Adminhtml\Question;

use Magento\Backend\App\Action;
use Magebit\Faq\Api\QuestionRepositoryInterface;
use Magebit\Faq\Api\Data\QuestionInterface;

/**
 * Class Delete
 * @package Magebit\Faq\Controller\Adminhtml\Question
 */
class Delete extends Action
{
    /**
     * @var QuestionRepositoryInterface
     */
    protected $questionRepository;

    /**
     * Delete constructor.
     * @param Action\Context $context
     * @param QuestionRepositoryInterface $questionRepository
     */
    public function __construct(
        Action\Context $context,
        QuestionRepositoryInterface $questionRepository
    )
    {
        parent::__construct($context);
        $this->questionRepository = $questionRepository;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $selectedQuestion = (int) $this->getRequest()->getParam(QuestionInterface::ID);

        try {
            $this->questionRepository->deleteById($selectedQuestion);
            $this->messageManager->addSuccess(__('The question has been deleted.'));
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }

        return $this->resultRedirectFactory->create()->setPath('*/*/');
    }
}