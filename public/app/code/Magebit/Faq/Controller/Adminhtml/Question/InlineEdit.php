<?php
declare(strict_types=1);

namespace Magebit\Faq\Controller\Adminhtml\Question;

use Magento\Backend\App\Action;
use Magebit\Faq\Api\QuestionRepositoryInterface;
use Magento\Framework\Controller\Result\JsonFactory;

/**
 * Class InlineEdit
 * @package Magebit\Faq\Controller\Adminhtml\Question
 */
class InlineEdit extends Action
{
    /**
     * @var QuestionRepositoryInterface
     */
    protected $questionRepository;

    /**
     * @var JsonFactory
     */
    protected $jsonFactory;

    /**
     * InlineEdit constructor.
     * @param Action\Context $context
     * @param QuestionRepositoryInterface $questionRepository
     * @param JsonFactory $jsonFactory
     */
    public function __construct(Action\Context $context,
                                QuestionRepositoryInterface $questionRepository,
                                JsonFactory $jsonFactory)
    {
        parent::__construct($context);
        $this->questionRepository = $questionRepository;
        $this->jsonFactory = $jsonFactory;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Json|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /**
         * @var \Magento\Framework\Controller\Result\Json $resultJson
         */
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $message = [];
        if ($this->getRequest()->getParam('isAjax')) {
            $postItem = $this->getRequest()->getParam('items', []);

            if (!count($postItem)) {
                $message[] = __('Wrong data');
                $error = true;
            } else {
                foreach (array_keys($postItem) as $modelId) {
                    $model = $this->questionRepository->getById($modelId);
                    try {
                        $model->setData(array_merge($model->getData(), $postItem[$modelId]));
                        $this->questionRepository->save($model);
                    } catch (\Exception $e) {
                        $message[] = $e->getMessage();
                        $error = true;
                    }
                }
            }
        }

        return $resultJson->setData([
            'messages' => $message,
            'error' => $error
        ]);
    }
}