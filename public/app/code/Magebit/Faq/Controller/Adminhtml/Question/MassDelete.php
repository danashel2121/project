<?php
declare(strict_types=1);

namespace Magebit\Faq\Controller\Adminhtml\Question;

use Magento\Backend\App\Action;
use Magebit\Faq\Model\ResourceModel\Question\CollectionFactory;
use Magento\Backend\Model\View\Result\RedirectFactory;
use Magento\Ui\Component\MassAction\Filter;
use Magebit\Faq\Model\QuestionRepository;
use Magento\Framework\App\Action\HttpPostActionInterface;

/**
 * Class MassDelete
 * @package Magebit\Faq\Controller\Adminhtml\Question
 */
class MassDelete extends Action implements HttpPostActionInterface
{
    /**
     * @var QuestionRepository
     */
    protected $questionRepository;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var RedirectFactory
     */
    protected $redirectFactory;

    /**
     * MassDelete constructor.
     * @param Action\Context $context
     * @param QuestionRepository $questionRepository
     * @param CollectionFactory $collectionFactory
     * @param Filter $filter
     */
    public function __construct(
        Action\Context $context,
        QuestionRepository $questionRepository,
        CollectionFactory $collectionFactory,
        Filter $filter,
        RedirectFactory $redirectFactory
    )
    {
        parent::__construct($context);
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->questionRepository = $questionRepository;
        $this->redirectFactory = $redirectFactory;

    }

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        foreach ($collection as $question) {
            $this->questionRepository->delete($question);
        }
        $this->messageManager->addSuccess(__('Selected questions has been deleted.'));
        return $this->redirectFactory->create()->setPath('*/*/');

    }
}