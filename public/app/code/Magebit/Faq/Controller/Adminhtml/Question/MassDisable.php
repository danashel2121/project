<?php
declare(strict_types=1);

namespace Magebit\Faq\Controller\Adminhtml\Question;

use Magento\Backend\App\Action;
use Magebit\Faq\Model\ResourceModel\Question\CollectionFactory;
use Magento\Backend\Model\View\Result\RedirectFactory;
use Magento\Ui\Component\MassAction\Filter;
use Magebit\Faq\Api\QuestionManagementInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;

/**
 * Class MassDisable
 * @package Magebit\Faq\Controller\Adminhtml\Question
 */
class MassDisable extends Action implements HttpPostActionInterface
{
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var QuestionManagementInterface
     */
    protected $questionManagement;

    /**
     * @var RedirectFactory
     */
    protected $redirectFactory;

    /**
     * MassDisable constructor.
     * @param Action\Context $context
     * @param CollectionFactory $collectionFactory
     * @param Filter $filter
     * @param QuestionManagementInterface $questionManagement
     * @param RedirectFactory $redirectFactory
     */
    public function __construct(
        Action\Context $context,
        CollectionFactory $collectionFactory,
        Filter $filter,
        QuestionManagementInterface $questionManagement,
        RedirectFactory $redirectFactory
    )
    {
        parent::__construct($context);

        $this->questionManagement = $questionManagement;
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->redirectFactory = $redirectFactory;
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        foreach ($collection as $question) {
            $this->questionManagement->disableQuestion($question);
        }
        $this->messageManager->addSuccess(__('Selected questions has been disabled.'));
        return $this->redirectFactory->create()->setPath('*/*/');
    }

}