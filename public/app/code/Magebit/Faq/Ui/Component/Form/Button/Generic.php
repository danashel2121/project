<?php
declare(strict_types=1);
namespace Magebit\Faq\Ui\Component\Form\Button;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
use  Magento\Backend\Block\Widget\Context;
use Magebit\Faq\Model\QuestionRepository;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class Generic
 * @package Magebit\Faq\Ui\Component\Form\Button
 */
class Generic implements ButtonProviderInterface
{
    protected $urlBuilder;
    protected $questionRepository;
    protected $request;

    /**
     * Generic constructor.
     * @param Context $context
     * @param QuestionRepository $questionRepository
     * @param RequestInterface $request
     */
    public function __construct(
        Context $context,
        QuestionRepository $questionRepository,
        RequestInterface $request
    )
    {
        $this->urlBuilder = $context->getUrlBuilder();
        $this->questionRepository = $questionRepository;
        $this->request = $request;
    }

    /**
     * @return array
     */
    public function getButtonData()
    {
        return [];
    }

    /**
     * @return int|null
     */
    public function getQuestionId(): ?int
    {
        $id = (int) $this->request->getParam('id');
        try {
            $this->questionRepository->getById($id);

            return $id;
            } catch (NoSuchEntityException $e) {
        }
        return null;
    }

    /**
     * @param string $route
     * @param array $param
     * @return string
     */
    public function getUrl($route = '', $param = []): string
    {
        return $this->urlBuilder->getUrl($route, $param);
    }

}