<?php
declare(strict_types=1);
namespace Magebit\Faq\Model\ResourceModel\Question;

use Magebit\Faq\Model\Question;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magebit\Faq\Model\ResourceModel\Question as QuestionResource;

/**
 * Class Collection
 * @package Magebit\Faq\Model\ResourceModel\Question
 */
class Collection extends AbstractCollection
{
    public function _construct()
    {
        $this->_init(Question::class, QuestionResource::class);
    }
}