<?php
declare(strict_types=1);
namespace Magebit\Faq\Model\Question\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Magebit\Faq\Api\Data\QuestionInterface;

class QuestionLayout implements OptionSourceInterface
{
    /**
     * @return array
     */
    public function toOptionArray(): array
    {
        return [
            ['value' => QuestionInterface::ENABLED, 'label' => __('Enabled')],
            ['value' => QuestionInterface::DISABLED, 'label' => __('Disabled')]
        ];

    }
}
